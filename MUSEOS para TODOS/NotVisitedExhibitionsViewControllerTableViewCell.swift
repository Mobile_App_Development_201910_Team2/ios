//
//  NotVisitedExhibitionsViewControllerTableViewCell.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit

class NotVisitedExhibitionsViewControllerTableViewCell: UITableViewCell {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var exhibitionName: UILabel!
    @IBOutlet weak var exhibitionImage: UIImageView!
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------

    func setExhibition(exhibition: Exhibition)
    {
        exhibitionName.text = exhibition.name
        exhibitionImage.image = exhibition.image
    }
}
