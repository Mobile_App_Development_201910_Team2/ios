//
//  TextToSpeechViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import QuartzCore
import AVFoundation

class TextToSpeechViewController: UIViewController, AVSpeechSynthesizerDelegate, UITextViewDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var textoAVozTextView: UITextView!
    @IBOutlet weak var pausarButton: UIButton!
    @IBOutlet weak var hablarButton: UIButton!
    @IBOutlet weak var pararButton: UIButton!
    @IBOutlet weak var textoAVozProgressView: UIProgressView!
    @IBOutlet weak var idiomaSegmentedControl: UISegmentedControl!
    @IBOutlet weak var textoAVozNavigationBar: UINavigationBar!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    let speechSynthesizer = AVSpeechSynthesizer()
    var rate: Float!
    var pitch: Float!
    var volume: Float!
    var totalUtterances: Int! = 0
    var currentUtterance: Int! = 0
    var totalTextLength: Int = 0
    var spokenTextLengths: Int = 0
    var preferredVoiceLanguageCode: String!
    var previousSelectedRange: NSRange!
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    @IBAction func pausarButtonAction(_ sender: UIButton)
    {
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
        animateActionButtonAppearance(shouldHideSpeakButton: false)
    }
    
    @IBAction func hablarButtonAction(_ sender: UIButton)
    {
        if validate(textView: textoAVozTextView)
        {
            var lang:String = "es-ES"
            switch idiomaSegmentedControl.selectedSegmentIndex {
            case 0:
                lang = "es-ES"
                break;
            case 1:
                lang = "en-US"
                break;
            case 2:
                lang = "fr-FR"
                break;
            case 3:
                lang = "de-DE"
                break;
            case 4:
                lang = "it-IT"
                break;
            default:
                lang = "es-ES"
                break;
            }
            
            if !speechSynthesizer.isSpeaking {
                let textParagraphs = textoAVozTextView.text.components(separatedBy: "\n")
                
                totalUtterances = textParagraphs.count
                currentUtterance = 0
                totalTextLength = 0
                spokenTextLengths = 0
                
                for pieceOfText in textParagraphs {
                    let speechUtterance = AVSpeechUtterance(string: pieceOfText)
                    speechUtterance.voice = AVSpeechSynthesisVoice(language: lang)
                    speechUtterance.rate = rate
                    speechUtterance.pitchMultiplier = pitch
                    speechUtterance.volume = volume
                    speechUtterance.postUtteranceDelay = 0.005
                    
                    if let voiceLanguageCode = preferredVoiceLanguageCode {
                        let voice = AVSpeechSynthesisVoice(language: voiceLanguageCode)
                        speechUtterance.voice = voice
                    }
                    
                    totalTextLength = totalTextLength + pieceOfText.utf16.count
                    
                    speechSynthesizer.speak(speechUtterance)
                }
            }
            else
            {
                speechSynthesizer.continueSpeaking()
            }
            
            animateActionButtonAppearance(shouldHideSpeakButton: true)
        }
        else
        {
            self.createEscribirAlgoEnTextViewAlert(title: "Texto a Voz", message:"Por favor escribe algo para que la función de Texto a Voz funcione.")
        }
    }
    
    @IBAction func pararButtonAction(_ sender: UIButton)
    {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
        animateActionButtonAppearance(shouldHideSpeakButton: false)
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textoAVozNavigationBar.barTintColor = UserDefaults.standard.colorForKey(key: "Color")
        idiomaSegmentedControl.tintColor = UserDefaults.standard.colorForKey(key: "Color")
        
        textoAVozTextView.delegate = self
        
        pausarButton.layer.cornerRadius = 20
        hablarButton.layer.cornerRadius = 20
        pararButton.layer.cornerRadius = 20
        pausarButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        hablarButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        pararButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        
        pausarButton.alpha = 0.0
        pararButton.alpha = 0.0
        
        textoAVozProgressView.alpha = 0.0
        textoAVozProgressView.progress = 0.0
        
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipeDownGesture:")))
        swipeDownGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.down
        view.addGestureRecognizer(swipeDownGestureRecognizer)
        
        if !loadSettings() {
            registerDefaultSettings()
        }
        
        speechSynthesizer.delegate = self
        
        setInitialFontAttribute()
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        textoAVozTextView.text = "Aquí puedes escribir lo que quieres que se diga."

        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "No hay conexión a Internet", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleSwipeDownGesture(gestureRecognizer: UISwipeGestureRecognizer) {
        textoAVozTextView.resignFirstResponder()
    }
    
    func registerDefaultSettings() {
        rate = AVSpeechUtteranceDefaultSpeechRate
        pitch = 1.0
        volume = 1.0
        
        let defaultSpeechSettings: Dictionary<String, AnyObject> = ["rate": rate as AnyObject, "pitch": pitch as AnyObject, "volume": volume as AnyObject]
        
        UserDefaults.standard.register(defaults: defaultSpeechSettings)
    }
    
    func loadSettings() -> Bool {
        let userDefaults = UserDefaults.standard as UserDefaults
        
        if let theRate: Float = userDefaults.value(forKey: "rate") as? Float {
            rate = theRate
            pitch = userDefaults.value(forKey: "pitch") as? Float
            volume = userDefaults.value(forKey: "volume") as? Float
            
            return true
        }
        
        return false
    }
    
    func animateActionButtonAppearance(shouldHideSpeakButton: Bool) {
        var speakButtonAlphaValue: CGFloat = 1.0
        var pauseStopButtonsAlphaValue: CGFloat = 0.0
        
        if shouldHideSpeakButton {
            speakButtonAlphaValue = 0.0
            pauseStopButtonsAlphaValue = 1.0
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.hablarButton.alpha = speakButtonAlphaValue
            self.pausarButton.alpha = pauseStopButtonsAlphaValue
            self.pararButton.alpha = pauseStopButtonsAlphaValue
            self.textoAVozProgressView.alpha = pauseStopButtonsAlphaValue
        })
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        spokenTextLengths = spokenTextLengths + utterance.speechString.utf16.count + 1
        
        let progress: Float = Float(spokenTextLengths * 100 / totalTextLength)
        textoAVozProgressView.progress = progress / 100
        
        if currentUtterance == totalUtterances {
            animateActionButtonAppearance(shouldHideSpeakButton: false)
        }
        
        if currentUtterance == totalUtterances {
            unselectLastWord()
            previousSelectedRange = nil
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        currentUtterance = currentUtterance + 1
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let progress: Float = Float(spokenTextLengths + characterRange.location) * 100 / Float(totalTextLength)
        textoAVozProgressView.progress = progress / 100
        
        let rangeInTotalText = NSMakeRange(spokenTextLengths + characterRange.location, characterRange.length)
        
        textoAVozTextView.selectedRange = rangeInTotalText
        
        let currentAttributes = textoAVozTextView.attributedText.attributes(at: rangeInTotalText.location, effectiveRange: nil)
        let fontAttribute: AnyObject? = currentAttributes[NSAttributedString.Key.font] as AnyObject?
        
        let attributedString = NSMutableAttributedString(string: textoAVozTextView.attributedText.attributedSubstring(from: rangeInTotalText).string)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.orange, range: NSMakeRange(0, attributedString.length))
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, attributedString.string.utf16.count))
        
        textoAVozTextView.scrollRangeToVisible(rangeInTotalText)
        
        textoAVozTextView.textStorage.beginEditing()
        
        textoAVozTextView.textStorage.replaceCharacters(in: rangeInTotalText, with: attributedString)
        
        if let previousRange = previousSelectedRange {
            let previousAttributedText = NSMutableAttributedString(string: textoAVozTextView.attributedText.attributedSubstring(from: previousRange).string)
            previousAttributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, previousAttributedText.length))
            previousAttributedText.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, previousAttributedText.length))
            
            textoAVozTextView.textStorage.replaceCharacters(in: previousRange, with: previousAttributedText)
        }
        
        textoAVozTextView.textStorage.endEditing()
        
        previousSelectedRange = rangeInTotalText
    }
    
    func didSaveSettings() {
        let settings = UserDefaults.standard as UserDefaults
        
        rate = settings.value(forKey: "rate") as? Float
        pitch = settings.value(forKey: "pitch") as? Float
        volume = settings.value(forKey: "volume") as? Float
        
        preferredVoiceLanguageCode = settings.object(forKey: "languageCode") as? String
    }
    
    func setInitialFontAttribute() {
        let rangeOfWholeText = NSMakeRange(0, textoAVozTextView.text.utf16.count)
        let attributedText = NSMutableAttributedString(string: textoAVozTextView.text)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica", size: 18.0)!, range: rangeOfWholeText)
        textoAVozTextView.textStorage.beginEditing()
        textoAVozTextView.textStorage.replaceCharacters(in: rangeOfWholeText, with: attributedText)
        textoAVozTextView.textStorage.endEditing()
    }
    
    func unselectLastWord() {
        if let selectedRange = previousSelectedRange {
            let currentAttributes = textoAVozTextView.attributedText.attributes(at: selectedRange.location, effectiveRange: nil)
            let fontAttribute: AnyObject? = currentAttributes[NSAttributedString.Key.font] as AnyObject?
            
            let attributedWord = NSMutableAttributedString(string: textoAVozTextView.attributedText.attributedSubstring(from: selectedRange).string)
            
            attributedWord.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, attributedWord.length))
            attributedWord.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, attributedWord.length))
            
            textoAVozTextView.textStorage.beginEditing()
            textoAVozTextView.textStorage.replaceCharacters(in: selectedRange, with: attributedWord)
            textoAVozTextView.textStorage.endEditing()
        }
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func textView(_ textoAVozTextView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textoAVozTextView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func createEscribirAlgoEnTextViewAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.textoAVozTextView.text = "Aquí puedes escribir todo lo que quieres que se diga."
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func validate(textView textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                return false
        }
        
        return true
        
    }
}
