//
//  CustomSlider.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit

class CustomSlider: UISlider {
    
    var sliderIdentifier: Int!
    
    required init() {
        super.init(frame: CGRect.zero)
        
        sliderIdentifier = 0
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        sliderIdentifier = 0
    }

}
