//
//  NotVisitedExhibitionsViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import FirebaseFirestore
import AVFoundation

class NotVisitedExhibitionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var notVisitedExhibitionsTableView: UITableView!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var audioPlayer: AVAudioPlayer?
    var colRef: CollectionReference!
    var exhibitionInformationListener: ListenerRegistration!
    var notVisitedExhibitions:[Exhibition] = []
    var newExhibition:Exhibition! = Exhibition(image: #imageLiteral(resourceName: "exhibition1"), name: "example")
    var museum: String = ""
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLoadingAlert(Message: "Los datos de las exhibiciones están cargando.")
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "notVisitedExhibitionsAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "exhibiciones_que_no_has_visitado.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "notVisitedExhibitionsAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        notVisitedExhibitionsTableView.dataSource = self
        notVisitedExhibitionsTableView.delegate = self
        
        colRef = Firestore.firestore().collection("/MuseosPT/\(museum)/Exposiciones")
        
        colRef.getDocuments { (snapshot, error) in
            if error != nil
            {
                print(error)
            }
            else
            {
                for document in (snapshot?.documents)!
                {
                    if let exhibitionName = document.data()["nombre"] as? String
                    {
                        let museumImageString = document.data()["thumbnail"] as? String
                        let url = URL(string: museumImageString!)
                        let data = NSData(contentsOf: url!)
                        if data != nil
                        {
                            self.newExhibition = Exhibition(image: UIImage(data: data! as Data)!, name: exhibitionName)
                            
                            // Check if user visited exhibition
                            var userVisitedExhibition = UserDefaults.standard.bool(forKey: "userVisitedExhibition")
                            
                            if !userVisitedExhibition
                            {
                                self.notVisitedExhibitions.append(self.newExhibition)
                                self.notVisitedExhibitionsTableView.reloadSections([0], with: UITableView.RowAnimation.fade)
                            }
                        }
                    }
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "exhibiciones_que_no_has_visitado.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    //  Functions
    //--------------------------------------------------
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notVisitedExhibitions.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let exhibition = notVisitedExhibitions[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NotVisitedExhibitionsViewControllerTableViewCell
        cell.setExhibition(exhibition: exhibition)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("section: \(indexPath.section)")
        print("row: \(indexPath.row)")
        let selectedExhibition = notVisitedExhibitions[indexPath.row]
        print("clicked: \(selectedExhibition.name)")
        
    }
    
    func createLoadingAlert(Message: String)
    {
        
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
}
