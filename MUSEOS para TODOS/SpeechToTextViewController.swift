//
//  SpeechToTextViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import Speech
import FirebaseFirestore

class SpeechToTextViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var empezarButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dictadoATextoNavigationBar: UINavigationBar!
    @IBOutlet weak var atrasButton: UIBarButtonItem!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    // Handles the speech recognition requests. Provides audio input to speech recognizer
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    
    // Recognition task
    private var recognitionTask: SFSpeechRecognitionTask?
    
    // Audio engine
    private let audioEngine = AVAudioEngine()
    
    // Language
    var lang: String = "es-ES"
    
    // SFSpeechRecognizer instance that handles speech recognition. "es-ES" is the user's language
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
    
    var alreadyRatedExhibition = false
    var colRef: CollectionReference!
    var museum: String = ""
    var exhibition: String = ""
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    @IBAction func segmentControlAction(_ sender: UISegmentedControl)
    {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            lang = "es-ES"
            break;
        case 1:
            lang = "en-US"
            break;
        case 2:
            lang = "fr-FR"
            break;
        case 3:
            lang = "de-DE"
            break;
        case 4:
            lang = "it-IT"
            break;
        default:
            lang = "es-ES"
            break;
        }
        
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
    }
    
    @IBAction func empezarButtonAction(_ sender: UIButton)
    {
        if audioEngine.isRunning
        {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            empezarButton.isEnabled = false
            empezarButton.setTitle("Empezar", for: .normal)
        }
        else
        {
            startRecording()
            empezarButton.setTitle("Parar", for: .normal)
        }
    }
    
    @IBAction func atrasButtonAction(_ sender: UIBarButtonItem) {
        
        if !alreadyRatedExhibition
        {
            self.createAlert(title: "Califica ésta exhibición", message:"De 1 a 5, donde 5 es el puntaje máximo, ¿con cuánto calificarías a ésta exhibición?")
        }
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "userVisitedExhibition")
        
        colRef = Firestore.firestore().collection("MuseosPT").document(museum).collection("Exposiciones").document(exhibition).collection("Calificaciones")
        
        dictadoATextoNavigationBar.barTintColor = UserDefaults.standard.colorForKey(key: "Color")
        segmentedControl.tintColor = UserDefaults.standard.colorForKey(key: "Color")
        
        empezarButton.layer.cornerRadius = 20
        empezarButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        
        empezarButton.setTitle("Empezar", for: .normal)
        
        // empezarButton disabled until speech recognizer is activated
        empezarButton.isEnabled = false
        
        // Sets the speech recognizer delegate
        speechRecognizer?.delegate = self

        // Requests authorization of Speech Recognition
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            // Checks the status of the verification
            switch authStatus {
            case . authorized:
                // Enable button that turns on microphone
                isButtonEnabled = true
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.empezarButton.isEnabled = isButtonEnabled
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")                
            }
        }
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func startRecording()
    {
        
        // Checks if recognitionTask is running
        if recognitionTask != nil
        {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        // Create an AVAudioSession
        let audioSession = AVAudioSession.sharedInstance()
        do
        {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .measurement, options: .defaultToSpeaker)
            //try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .measurement, options: .defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        }
        catch
        {
            print("audioSession properties weren't set because of an error.")
        }
        
        // A SFSpeechAudioBufferRecognitionRequest is created
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        // Checks if the audio engine has an audio input
        let inputNode = audioEngine.inputNode
        
        // Checks if the recognition request is not nil
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        // Tells recognition request to report partial results of speech recognition as the user speaks
        recognitionRequest.shouldReportPartialResults = true
        
        // Starts the recognition
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            // Boolean variable that tells if recognition is final
            var isFinal = false
            
            if result != nil {
                
                // If the result is not nil,  set the text view's text as the result's best transcription
                self.textView.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.empezarButton.isEnabled = true
            }
        })
        
        // Add audio input to the recognition request
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do
        {
            try audioEngine.start()
        }
        catch
        {
            print("audioEngine couldn't start because of an error.")
        }
        
        textView.text = "Acércate a alguien que esté hablando"
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool)
    {
        if available {
            empezarButton.isEnabled = true
        } else {
            empezarButton.isEnabled = false
        }
    }
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // 5
        alert.addAction(UIAlertAction(title: "5", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UIDevice.current.identifierForVendor!.uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 5,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        // 4
        alert.addAction(UIAlertAction(title: "4", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 4,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        // 3
        alert.addAction(UIAlertAction(title: "3", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 3,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        // 2
        alert.addAction(UIAlertAction(title: "2", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 2,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        // 1
        alert.addAction(UIAlertAction(title: "1", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 1,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        // No quiero calificar la exhibición, gracias
        alert.addAction(UIAlertAction(title: "No quiero calificar la exhibición, gracias", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            self.performSegue(withIdentifier: "speechToTextAtras", sender: self)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}
