//
//  Exhibition.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import Foundation
import UIKit

class Exhibition {
    
    var image: UIImage
    var name: String
    
    init(image: UIImage, name: String)
    {
        self.image = image
        self.name = name
    }
}
