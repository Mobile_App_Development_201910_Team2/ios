//
//  MuseumsViewControllerTableViewCell.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit

class MuseumsViewControllerTableViewCell: UITableViewCell {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------

    @IBOutlet weak var museumName: UILabel!
    @IBOutlet weak var museumImage: UIImageView!
    @IBOutlet weak var museumBackgroundView: UIView!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func layoutSubviews() {
        primaryColor = UserDefaults.standard.colorForKey(key: "Color")!
        museumBackgroundView.backgroundColor = primaryColor
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func setMuseum(museum: Museum)
    {
        museumName.text = museum.name
        museumImage.image = museum.image
    }
}
