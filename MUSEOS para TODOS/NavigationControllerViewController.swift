//
//  NavigationControllerViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit

class NavigationControllerViewController: UINavigationController {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet var museosNavigationBar: UINavigationBar!
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        museosNavigationBar.barTintColor = UserDefaults.standard.colorForKey(key: "Color")

    }
}
