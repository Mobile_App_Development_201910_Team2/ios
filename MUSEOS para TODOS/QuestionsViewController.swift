//
//  QuestionsViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import FirebaseFirestore

/*
 * View Controller for the Questions View
 */
class QuestionsViewController: UIViewController, AVAudioPlayerDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var noPuedoOirButton: UIButton!
    @IBOutlet weak var noReconozcoAlgunosColoresButton: UIButton!
    @IBOutlet weak var noPuedoVerButton: UIButton!
    @IBOutlet weak var continuarButton: UIButton!
    @IBOutlet weak var agePicker: UIPickerView!
    @IBOutlet weak var genderPicker: UIPickerView!
    @IBOutlet weak var question1Separator: UIView!
    @IBOutlet weak var question2Separator: UIView!
    @IBOutlet weak var question3Separator: UIView!
    @IBOutlet weak var userColorLabel: UILabel!
    @IBOutlet weak var preguntasNavigationBar: UINavigationBar!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    let ageGroups = ["15-25", "26-36", "37-47", "48-58", "59-69", "70-80", "81-91", "92 or above"]
    let gender = ["Masculino", "Femenino"]
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var audioPlayer: AVAudioPlayer?
    var selectedUserDisabilitySituation: String = ""
    var selectedAgeGroup: String?
    var selectedGender: String?
    var noPuedoOirButtonNumber = 2
    var noReconozcoAlgunosColoresButtonNumber = 2
    var noPuedoVerButtonNumber = 2
    var grayColor = UIColor.lightGray
    var orangeColor = UIColor.orange
    var userChosenColor:String = ""
    var primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
    var secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
    var colRef: CollectionReference!
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    /*
     * Action triggered when user clicks the continuar button
     */
    @IBAction func continuarButtonAction(_ sender: UIButton) {
                
        // Constants needed to save data into Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // Saves into a variable the user's disability situations
        if noPuedoOirButton.backgroundColor == primaryColor
        {
            selectedUserDisabilitySituation = "No puede oir"
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        }
        else if noReconozcoAlgunosColoresButton.backgroundColor == primaryColor
        {
            selectedUserDisabilitySituation = "No reconoce algunos colores \(userChosenColor)"
            
            if userChosenColor == " azul"
            {
                // Usuario con tritanopia
                primaryColor = #colorLiteral(red: 0.9098039216, green: 0.2078431373, blue: 0.1764705882, alpha: 1)
                secondaryColor = #colorLiteral(red: 0.9411764706, green: 0.6117647059, blue: 0.1882352941, alpha: 1)
            }
            else if userChosenColor == " rojo"
            {
                // Usuario con protanopia
                primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
                secondaryColor = #colorLiteral(red: 0.5450980392, green: 0.1882352941, blue: 0.7647058824, alpha: 1)
            }
            else if userChosenColor == " verde"
            {
                // Usuario con deuteranopia
                primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
                secondaryColor = #colorLiteral(red: 0.1764705882, green: 0.2745098039, blue: 0.9607843137, alpha: 1)
            }
        }
        else if noPuedoVerButton.backgroundColor == primaryColor
        {
            selectedUserDisabilitySituation = "No puede ver"
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            UserDefaults.standard.set(true, forKey: "UsuarioNoPuedeVer")
        }
        else
        {
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        }
        
        UserDefaults.standard.setColor(color: primaryColor, forKey: "Color")
        UserDefaults.standard.setColor(color: secondaryColor, forKey: "TabBarColor")
        
        // Constants used to save data into Core Data
        let userDisabilitySituation = selectedUserDisabilitySituation
        let userAge = ageGroups[agePicker.selectedRow(inComponent: 0)]
        let userGender = gender[genderPicker.selectedRow(inComponent: 0)]
        
        // Save user basic information into Firestore
        let idMobile = UIDevice.current.identifierForVendor!.uuidString
        let date = Date()
        let calendar = Calendar.current
        
        colRef.document("user-\(idMobile)").setData([
            "idMobile": idMobile,
            "disabilitySituation": userDisabilitySituation,
            "gender": userGender,
            "age": userAge,
            "day": calendar.component(.day, from: date),
            "month": calendar.component(.month, from: date),
            "year": calendar.component(.year, from: date)
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
        
        // Save user basic information into Core Data
        let newUserBasicInformation = NSEntityDescription.insertNewObject(forEntityName: "UserInformation", into: context)
        newUserBasicInformation.setValue(userDisabilitySituation, forKey: "disabilitySituation")
        newUserBasicInformation.setValue(userAge, forKey: "age")
        newUserBasicInformation.setValue(userGender, forKey: "gender")
         
        do
        {
            try context.save()
        }
        catch
        {
            print("The user's basic information could not be saved")
        }
        
        // Saves into UserDefaults userSelectedOptions
        UserDefaults.standard.set(true, forKey: "userSelectedOptions")
        
        // Performs segue
        self.performSegue(withIdentifier: "continuar", sender: self)
        
    }
    
    /*
     * Action triggered when user clicks the noPuedoOirButton
     */
    @IBAction func noPuedoOirButtonAction(_ sender: UIButton) {
        
        if noPuedoOirButtonNumber % 2 == 0
        {
            noPuedoOirButton.backgroundColor = primaryColor
            noReconozcoAlgunosColoresButton.backgroundColor = grayColor
            noReconozcoAlgunosColoresButtonNumber = 2
            userColorLabel.text = "No puedo ver el color"
            userColorLabel.isHidden = true
            noPuedoVerButton.backgroundColor = grayColor
            noPuedoVerButtonNumber = 2
        }
        else
        {
            noPuedoOirButton.backgroundColor = grayColor
        }
        
        noPuedoOirButtonNumber = noPuedoOirButtonNumber + 1

    }
    
    /*
     * Action triggered when user clicks the noReconozcoAlgunosColoresButton
     */
    @IBAction func noReconozcoAlgunosColoresButtonAction(_ sender: UIButton) {
        
        if noReconozcoAlgunosColoresButtonNumber % 2 == 0
        {
            noPuedoOirButton.backgroundColor = grayColor
            noPuedoOirButtonNumber = 2
            noPuedoVerButton.backgroundColor = grayColor
            noPuedoVerButtonNumber = 2
            
            createAlert(title: "Color", message: "Selecciona un color")
        }
        else
        {
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            
            noReconozcoAlgunosColoresButton.backgroundColor = grayColor
            noReconozcoAlgunosColoresButton.backgroundColor = UIColor.lightGray
            question1Separator.backgroundColor = primaryColor
            question2Separator.backgroundColor = primaryColor
            question3Separator.backgroundColor = primaryColor
            preguntasNavigationBar.barTintColor = primaryColor
            continuarButton.backgroundColor = secondaryColor
            userColorLabel.text = "No puedo ver el color"
            userColorLabel.isHidden = true
        }
        
        noReconozcoAlgunosColoresButtonNumber = noReconozcoAlgunosColoresButtonNumber + 1
        
    }
    
    /*
     * Action triggered when user clicks the noPuedoVerButton
     */
    @IBAction func noPuedoVerButton(_ sender: UIButton) {
        
        if noPuedoVerButtonNumber % 2 == 0
        {
            noPuedoVerButton.backgroundColor = primaryColor
            noPuedoOirButton.backgroundColor = grayColor
            noPuedoOirButtonNumber = 2
            noReconozcoAlgunosColoresButton.backgroundColor = grayColor
            noReconozcoAlgunosColoresButtonNumber = 2
            userColorLabel.text = "No puedo ver el color"
            userColorLabel.isHidden = true
        }
        else
        {
            noPuedoVerButton.backgroundColor = grayColor
        }
        
        noPuedoVerButtonNumber = noPuedoVerButtonNumber + 1
        
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        colRef = Firestore.firestore().collection("Usuarios")
        
        noPuedoOirButton.layer.cornerRadius = 20
        noReconozcoAlgunosColoresButton.layer.cornerRadius = 20
        noPuedoVerButton.layer.cornerRadius = 20
        continuarButton.layer.cornerRadius = 15
        continuarButton.backgroundColor = primaryColor
        
        createAgePicker()
        createGenderPicker()
        createToolbar()
        
        grayColor = noPuedoOirButton.backgroundColor!
        orangeColor = question1Separator.backgroundColor!
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
        
        if !(UserDefaults.standard.object(forKey: "questionsAudioWasAlreadyPlayed") != nil)
        {
            let path = Bundle.main.path(forResource: "preguntas.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "questionsAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }            
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            Alert(Message: "No hay conexión a Internet. Como es la primera vez que usas la app, no todo el contenido está disponible y se requiere de una conexión a Internet para descargar los datos de la app. Por favor, conéctate a Internet lo más pronto para disfrutar de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "preguntas.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func changeColors()
    {
        if userChosenColor == " azul"
        {
            // Usuario con tritanopia
            primaryColor = #colorLiteral(red: 0.9098039216, green: 0.2078431373, blue: 0.1764705882, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.9411764706, green: 0.6117647059, blue: 0.1882352941, alpha: 1)
        }
        else if userChosenColor == " rojo"
        {
            // Usuario con protanopia
            primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.5450980392, green: 0.1882352941, blue: 0.7647058824, alpha: 1)
        }
        else if userChosenColor == " verde"
        {
            // Usuario con deuteranopia
            primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.1764705882, green: 0.2745098039, blue: 0.9607843137, alpha: 1)
        }
        
        question1Separator.backgroundColor = primaryColor
        question2Separator.backgroundColor = primaryColor
        question3Separator.backgroundColor = primaryColor
        noReconozcoAlgunosColoresButton.backgroundColor = primaryColor
        continuarButton.backgroundColor = secondaryColor
        preguntasNavigationBar.barTintColor = primaryColor
    }
    
    /*
     * Sets age picker delegate
     */
    func createAgePicker() {
        agePicker.delegate = self
    }
    
    /*
     * Sets gender picker delegate
     */
    func createGenderPicker() {
        genderPicker.delegate = self
    }
    
    /*
     * Creates toolbar
     */
    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(QuestionsViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // Azul
        alert.addAction(UIAlertAction(title: "Azul", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " azul"
            self.userColorLabel.text = self.userColorLabel.text! + self.userChosenColor
            self.userColorLabel.isHidden = false
            self.changeColors()
        }))
        
        // Rojo
        alert.addAction(UIAlertAction(title: "Rojo", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " rojo"
            self.userColorLabel.text = self.userColorLabel.text! + self.userChosenColor
            self.userColorLabel.isHidden = false
            self.changeColors()
        }))
        
        // Verde
        alert.addAction(UIAlertAction(title: "Verde", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " verde"
            self.userColorLabel.text = self.userColorLabel.text! + self.userChosenColor
            self.userColorLabel.isHidden = false
            self.changeColors()
        }))
        
        // Cancelar
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.noReconozcoAlgunosColoresButton.backgroundColor = self.grayColor
            self.userColorLabel.text = "No puedo ver el color"
            self.userColorLabel.isHidden = true
            self.noReconozcoAlgunosColoresButtonNumber = self.noReconozcoAlgunosColoresButtonNumber + 1
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension QuestionsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return ageGroups.count
        }
        else
        {
            return gender.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1
        {
            return ageGroups[row]
        }
        else
        {
            return gender[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component:Int) {
        if pickerView.tag == 1
        {
            selectedAgeGroup = ageGroups[row]
        }
        else
        {
            selectedGender = gender[row]
        }
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet. Como es la primera vez que usas la app, no todo el contenido está disponible y se requiere de una conexión a Internet para descargar los datos de la app. Por favor, conéctate a Internet lo más pronto para disfrutar de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
}

extension UserDefaults {
    func colorForKey(key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = data(forKey: key) {
            color = NSKeyedUnarchiver.unarchiveObject(with: colorData) as? UIColor
        }
        return color
    }
    
    func setColor(color: UIColor?, forKey key: String) {
        var colorData: NSData?
        if let color = color {
            colorData = NSKeyedArchiver.archivedData(withRootObject: color) as NSData?
        }
        set(colorData, forKey: key)
    }
}
