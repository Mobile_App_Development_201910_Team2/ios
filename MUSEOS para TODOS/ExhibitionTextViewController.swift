//
//  ExhibitionTextViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseFirestore

class ExhibitionTextViewController: UIViewController {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var exhibitionName: UILabel!
    @IBOutlet weak var exhibitionImage: UIImageView!
    @IBOutlet weak var exhibitionText: UITextView!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var alreadyRatedExhibition = false
    var audioPlayer: AVAudioPlayer?
    var museum: String = ""
    var exhibition: String = ""
    var colRef: CollectionReference!
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    @IBAction func atrasButtonAction(_ sender: UIBarButtonItem) {
        
        if !alreadyRatedExhibition
        {
            self.createAlert(title: "Califica ésta exhibición", message:"De 1 a 5, donde 5 es el puntaje máximo, ¿con cuánto calificarías a ésta exhibición?")
        }
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "userVisitedExhibition")
        
        colRef = Firestore.firestore().collection("MuseosPT").document(museum).collection("Exposiciones").document(exhibition).collection("Calificaciones")
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "exhibitionTextAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "text.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "exhibitionTextAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "texto.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // 5
        alert.addAction(UIAlertAction(title: "5", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UIDevice.current.identifierForVendor!.uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 5,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        // 4
        alert.addAction(UIAlertAction(title: "4", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 4,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        // 3
        alert.addAction(UIAlertAction(title: "3", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 3,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        // 2
        alert.addAction(UIAlertAction(title: "2", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 2,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        // 1
        alert.addAction(UIAlertAction(title: "1", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 1,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        // No quiero calificar la exhibición, gracias
        alert.addAction(UIAlertAction(title: "No quiero calificar la exhibición, gracias", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            self.performSegue(withIdentifier: "textoAtras", sender: self)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
}
