//
//  ExhibitionAudioPlayerViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseFirestore

class ExhibitionAudioPlayerViewController: UIViewController, AVAudioPlayerDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var exhibitionNameLabel: UILabel!
    @IBOutlet weak var reproducirButton: UIButton!
    @IBOutlet weak var pausarButton: UIButton!
    @IBOutlet weak var pararButton: UIButton!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
    var alreadyRatedExhibition = false
    var audioPlayer: AVAudioPlayer?
    var museum: String = ""
    var exhibition: String = ""
    var colRef: CollectionReference!
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    @IBAction func reproducirButtonAction(_ sender: UIButton) {
        
        audioPlayer?.play()
        
    }
    
    
    @IBAction func pausarButtonAction(_ sender: UIButton) {
        
        audioPlayer?.pause()
        
    }
    
    
    @IBAction func pararButtonAction(_ sender: UIButton) {
        
        audioPlayer?.stop()
        audioPlayer?.currentTime = 0
        
    }
    
    @IBAction func atrasButtonAction(_ sender: UIBarButtonItem) {
        
        if !alreadyRatedExhibition
        {
            self.createAlert(title: "Califica ésta exhibición", message:"De 1 a 5, donde 5 es el puntaje máximo, ¿con cuánto calificarías a ésta exhibición?")
        }
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "userVisitedExhibition")
        
        colRef = Firestore.firestore().collection("MuseosPT").document(museum).collection("Exposiciones").document(exhibition).collection("Calificaciones")
        
        let path = Bundle.main.path(forResource: "audio_exposicion_banderas.wav", ofType: nil)!
        let url = URL(fileURLWithPath: path)
        
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        
        primaryColor = UserDefaults.standard.colorForKey(key: "Color")!
        
        reproducirButton.layer.cornerRadius = 20
        reproducirButton.backgroundColor = primaryColor
        pausarButton.layer.cornerRadius = 20
        pausarButton.backgroundColor = primaryColor
        pararButton.layer.cornerRadius = 20
        pararButton.backgroundColor = primaryColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // 5
        alert.addAction(UIAlertAction(title: "5", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UIDevice.current.identifierForVendor!.uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 5,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        // 4
        alert.addAction(UIAlertAction(title: "4", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 4,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }

            
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        // 3
        alert.addAction(UIAlertAction(title: "3", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 3,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        // 2
        alert.addAction(UIAlertAction(title: "2", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 2,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        // 1
        alert.addAction(UIAlertAction(title: "1", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            
            // Guardar información en Firestore
            let idMobile = UUID().uuidString
            let date = Date()
            let calendar = Calendar.current
            self.colRef.document("cal-\(idMobile)").setData([
                "idMobile": idMobile,
                "rating": 1,
                "day": calendar.component(.day, from: date),
                "month": calendar.component(.month, from: date),
                "year": calendar.component(.year, from: date)
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        // No quiero calificar la exhibición, gracias
        alert.addAction(UIAlertAction(title: "No quiero calificar la exhibición, gracias", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.alreadyRatedExhibition = true
            self.performSegue(withIdentifier: "audioPlayerAtras", sender: self)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}
