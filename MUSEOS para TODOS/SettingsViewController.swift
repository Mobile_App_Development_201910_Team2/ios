//
//  SettingsViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import FirebaseFirestore

class SettingsViewController: UIViewController {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var noPuedoOirButtonOutlet: UIButton!
    @IBOutlet weak var noReconozcoAlgunosColoresButtonOutlet: UIButton!
    @IBOutlet weak var noPuedoVerButtonOutlet: UIButton!
    @IBOutlet weak var guardarButtonOutlet: UIButton!
    @IBOutlet weak var noPuedoVerElColorLabelOutlet: UILabel!
    @IBOutlet weak var ajustesNavigationBar: UINavigationBar!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var userSavedDisabilitySituation:String = ""
    var userSavedAge:String = ""
    var userSavedGender:String = ""
    var noPuedoOirButtonNumber = 2
    var noReconozcoAlgunosColoresButtonNumber = 2
    var noPuedoVerButtonNumber = 2
    var userChosenColor:String = ""
    var selectedUserDisabilitySituation: String = ""
    var primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
    var secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
    var audioPlayer: AVAudioPlayer?
    var colRef: CollectionReference!
    
    //--------------------------------------------------
    // Actions
    //--------------------------------------------------
    
    @IBAction func noPuedoOirButtonAction(_ sender: UIButton) {
        
        if noPuedoOirButtonNumber % 2 == 0
        {
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                
            guardarButtonOutlet.backgroundColor = secondaryColor
            ajustesNavigationBar.barTintColor = primaryColor
            
            noPuedoOirButtonOutlet.backgroundColor = primaryColor
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noReconozcoAlgunosColoresButtonNumber = 2
            noPuedoVerElColorLabelOutlet.text = "No puedo ver el color"
            noPuedoVerElColorLabelOutlet.isHidden = true
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerButtonNumber = 2
        }
        else
        {
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
        }
        
        noPuedoOirButtonNumber = noPuedoOirButtonNumber + 1
        
    }
    
    @IBAction func noReconozcoAlgunosColoresButtonAction(_ sender: UIButton) {
        
        if noReconozcoAlgunosColoresButtonNumber % 2 == 0
        {
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoOirButtonNumber = 2
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerButtonNumber = 2
            
            createAlert(title: "Color", message: "Selecciona un color")
        }
        else
        {
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            guardarButtonOutlet.backgroundColor = primaryColor
            ajustesNavigationBar.barTintColor = primaryColor
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerElColorLabelOutlet.text = "No puedo ver el color"
            noPuedoVerElColorLabelOutlet.isHidden = true
        }
        
        noReconozcoAlgunosColoresButtonNumber = noReconozcoAlgunosColoresButtonNumber + 1
        
    }
    
    @IBAction func noPuedoVerButtonAction(_ sender: UIButton) {
        
        if noPuedoVerButtonNumber % 2 == 0
        {
            primaryColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.1098039216, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            
            guardarButtonOutlet.backgroundColor = secondaryColor
            ajustesNavigationBar.barTintColor = primaryColor
            
            noPuedoVerButtonOutlet.backgroundColor = primaryColor
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoOirButtonNumber = 2
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noReconozcoAlgunosColoresButtonNumber = 2
            noPuedoVerElColorLabelOutlet.text = "No puedo ver el color"
            noPuedoVerElColorLabelOutlet.isHidden = true
        }
        else
        {
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
        }
        
        noPuedoVerButtonNumber = noPuedoVerButtonNumber + 1
        
    }
    
    @IBAction func guardarButtonAction(_ sender: UIButton) {
        
        UserDefaults.standard.setColor(color: primaryColor, forKey: "Color")
        UserDefaults.standard.setColor(color: secondaryColor, forKey: "TabBarColor")
        
        // Save information into Core Data
        
        // Constants needed to save data into Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // Saves into a variable the user's disability situations
        if noPuedoOirButtonOutlet.backgroundColor == UserDefaults.standard.colorForKey(key: "Color")
        {
            selectedUserDisabilitySituation = "No puede oir"
        }
        else if noReconozcoAlgunosColoresButtonOutlet.backgroundColor == UserDefaults.standard.colorForKey(key: "Color")
        {
            selectedUserDisabilitySituation = "No reconoce algunos colores " + userChosenColor
        }
        else if noPuedoVerButtonOutlet.backgroundColor == UserDefaults.standard.colorForKey(key: "Color")
        {
            selectedUserDisabilitySituation = "No puede ver"
        }
        
        // Constants used to save data into Core Data
        let userDisabilitySituation = selectedUserDisabilitySituation
        
        // Save user basic information into Core Data
        let newUserBasicInformation = NSEntityDescription.insertNewObject(forEntityName: "UserInformation", into: context)
        newUserBasicInformation.setValue(userDisabilitySituation, forKey: "disabilitySituation")
        
        do
        {
            try context.save()
        }
        catch
        {
            print("The user's basic information could not be saved")
        }
        
        // Saves into UserDefaults userSelectedOptions
        UserDefaults.standard.set(true, forKey: "userSelectedOptions")
        
        // Save user basic information into Firestore
        let idMobile = UIDevice.current.identifierForVendor!.uuidString
        
        colRef = Firestore.firestore().collection("Usuarios")
        
        colRef.document("user-\(idMobile)").setData([
            "disabilitySituation": userDisabilitySituation,
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
        
        self.createGuardadoAlert(Message: "Tus ajustes se han guardado.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.performSegue(withIdentifier: "goToMuseums", sender: self)
        }
    }
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "settingsAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "ajustes.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "settingsAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        primaryColor = UserDefaults.standard.colorForKey(key: "Color")!
        secondaryColor = UserDefaults.standard.colorForKey(key: "TabBarColor")!
        
        ajustesNavigationBar.barTintColor = primaryColor
        
        noPuedoOirButtonOutlet.layer.cornerRadius = 20
        noReconozcoAlgunosColoresButtonOutlet.layer.cornerRadius = 20
        noPuedoVerButtonOutlet.layer.cornerRadius = 20
        guardarButtonOutlet.layer.cornerRadius = 20
        guardarButtonOutlet.backgroundColor = secondaryColor
        
        loadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        loadData()
        
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        noPuedoVerElColorLabelOutlet.text = ""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "ajustes.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func changeColors()
    {
        if userChosenColor == " azul"
        {
            // Usuario con tritanopia
            primaryColor = #colorLiteral(red: 0.9098039216, green: 0.2078431373, blue: 0.1764705882, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.9411764706, green: 0.6117647059, blue: 0.1882352941, alpha: 1)
        }
        else if userChosenColor == " rojo"
        {
            // Usuario con protanopia
            primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.5450980392, green: 0.1882352941, blue: 0.7647058824, alpha: 1)
        }
        else if userChosenColor == " verde"
        {
            // Usuario con deuteranopia
            primaryColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.3843137255, alpha: 1)
            secondaryColor = #colorLiteral(red: 0.1764705882, green: 0.2745098039, blue: 0.9607843137, alpha: 1)
        }
        
        guardarButtonOutlet.backgroundColor = secondaryColor
        ajustesNavigationBar.barTintColor = primaryColor
        noReconozcoAlgunosColoresButtonOutlet.backgroundColor = primaryColor
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // Azul
        alert.addAction(UIAlertAction(title: "Azul", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " azul"
            self.noPuedoVerElColorLabelOutlet.text = self.noPuedoVerElColorLabelOutlet.text! + self.userChosenColor
            self.noPuedoVerElColorLabelOutlet.isHidden = false
            self.changeColors()
        }))
        
        // Rojo
        alert.addAction(UIAlertAction(title: "Rojo", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " rojo"
            self.noPuedoVerElColorLabelOutlet.text = self.noPuedoVerElColorLabelOutlet.text! + self.userChosenColor
            self.noPuedoVerElColorLabelOutlet.isHidden = false
            self.changeColors()
        }))
        
        // Verde
        alert.addAction(UIAlertAction(title: "Verde", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.userChosenColor = " verde"
            self.noPuedoVerElColorLabelOutlet.text = self.noPuedoVerElColorLabelOutlet.text! + self.userChosenColor
            self.noPuedoVerElColorLabelOutlet.isHidden = false
            self.changeColors()
        }))
        
        // Cancelar
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            self.noPuedoVerElColorLabelOutlet.text = "No puedo ver el color"
            self.noPuedoVerElColorLabelOutlet.isHidden = true
            self.noReconozcoAlgunosColoresButtonNumber = self.noReconozcoAlgunosColoresButtonNumber + 1
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func createGuardadoAlert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadData()
    {
        // Constants needed to read from Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInformation")
        request.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(request)
            
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let userDisabilitySituation = result.value(forKey: "disabilitySituation") as? String
                    {
                        userSavedDisabilitySituation = userDisabilitySituation
                    }
                    if let userAge = result.value(forKey: "age") as? String
                    {
                        userSavedAge = userAge
                    }
                    if let userGender = result.value(forKey: "gender") as? String
                    {
                        userSavedGender = userGender
                    }
                }
            }
        }
        catch
        {
            print("Data could not be read from Core Data")
        }
        
        if userSavedDisabilitySituation.contains("No puede oir")
        {
            noPuedoOirButtonOutlet.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
            noPuedoOirButtonNumber = 3
            
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noReconozcoAlgunosColoresButtonNumber = 2
            
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerButtonNumber = 2
        }
        else if userSavedDisabilitySituation.contains("No reconoce algunos colores")
        {
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = primaryColor
            noReconozcoAlgunosColoresButtonNumber = 3
            
            if userSavedDisabilitySituation.contains("azul")
            {
                noPuedoVerElColorLabelOutlet.text = "No puedo ver el color" + " azul"
                noPuedoVerElColorLabelOutlet.isHidden = false
            }
            if userSavedDisabilitySituation.contains("rojo")
            {
                noPuedoVerElColorLabelOutlet.text = "No puedo ver el color" + " rojo"
                noPuedoVerElColorLabelOutlet.isHidden = false
            }
            if userSavedDisabilitySituation.contains("verde")
            {
                noPuedoVerElColorLabelOutlet.text = "No puedo ver el color" + " verde"
                noPuedoVerElColorLabelOutlet.isHidden = false
            }
            
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoOirButtonNumber = 2
            
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerButtonNumber = 2
        }
        else if userSavedDisabilitySituation.contains("No puede ver")
        {
            noPuedoVerButtonOutlet.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
            noPuedoVerButtonNumber = 3
            
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoOirButtonNumber = 2
            
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noReconozcoAlgunosColoresButtonNumber = 2
        }
        else
        {
            noPuedoVerButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoVerButtonNumber = 2
            
            noPuedoOirButtonOutlet.backgroundColor = UIColor.lightGray
            noPuedoOirButtonNumber = 2
            
            noReconozcoAlgunosColoresButtonOutlet.backgroundColor = UIColor.lightGray
            noReconozcoAlgunosColoresButtonNumber = 2
        }
    }
}
