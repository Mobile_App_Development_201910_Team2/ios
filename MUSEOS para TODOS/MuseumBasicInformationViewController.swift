//
//  MuseumBasicInformationViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import FirebaseFirestore
import CoreData
import AVFoundation

class MuseumBasicInformationViewController: UIViewController {

    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var museoTitle: UILabel!
    @IBOutlet weak var museoImage: UIImageView!
    @IBOutlet weak var museoRecommendedVisitTime: UILabel!
    @IBOutlet weak var museoDescription: UILabel!
    @IBOutlet weak var museoEntranceCost: UILabel!
    @IBOutlet weak var museoAccessibility: UILabel!
    @IBOutlet weak var museoAccessibility2: UILabel!
    @IBOutlet weak var museoHoursOfServiceMonday: UILabel!
    @IBOutlet weak var museoHoursOfServiceTuesday: UILabel!
    @IBOutlet weak var museoHoursOfServiceWednesday: UILabel!
    @IBOutlet weak var museoHoursOfServiceThursday: UILabel!
    @IBOutlet weak var museoHoursOfServiceFriday: UILabel!
    @IBOutlet weak var museoHoursOfServiceSaturday: UILabel!
    @IBOutlet weak var museoHoursOfServiceSunday: UILabel!
    @IBOutlet weak var separator1: UIView!
    @IBOutlet weak var separator2: UIView!
    @IBOutlet weak var separator3: UIView!
    @IBOutlet weak var separator4: UIView!
    @IBOutlet weak var separator5: UIView!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var audioPlayer: AVAudioPlayer?
    var docRef: DocumentReference!
    var museumInformationListener: ListenerRegistration!
    var selectedMuseum: String = ""
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("VIEW DID LOAD")
        
        selectedMuseum = String(selectedMuseumName.split(separator: "/")[1])
        
        print("SELECTED MUSEUM")
        print(selectedMuseum)
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "museumBasicInformationAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "acerca_de_este_museo.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "museumBasicInformationAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        separator1.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        separator2.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        separator3.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        separator4.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        separator5.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        
        docRef = Firestore.firestore().document(selectedMuseumName)
                
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        museumInformationListener = docRef.addSnapshotListener { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let myData = docSnapshot.data()
            
            let latestMuseoTitle = myData?["nombre"] as? String ?? ""
            let latestMuseoDescription = myData?["descripcion"] as? String ?? ""
            let latestMuseoHoursOfService = myData?["horario-atencion"] as? [String] ?? [""]
            let latestAccesibilidad = myData?["accesibilidad"] as? [String] ?? [""]
            let latestThumbnail = myData?["thumbnail"] as? String ?? ""
            let latestValorEntrada = myData?["valor-entrada"] as? String ?? ""
            
            self.museoTitle.text = latestMuseoTitle
            self.museoDescription.text = latestMuseoDescription
            self.museoHoursOfServiceMonday.text = latestMuseoHoursOfService[0]
            self.museoHoursOfServiceTuesday.text = latestMuseoHoursOfService[1]
            self.museoHoursOfServiceWednesday.text = latestMuseoHoursOfService[2]
            self.museoHoursOfServiceThursday.text = latestMuseoHoursOfService[3]
            self.museoHoursOfServiceFriday.text = latestMuseoHoursOfService[4]
            self.museoHoursOfServiceSaturday.text = latestMuseoHoursOfService[5]
            self.museoHoursOfServiceSunday.text = latestMuseoHoursOfService[6]
            self.museoAccessibility.text = latestAccesibilidad[0]
            self.museoAccessibility2.text = latestAccesibilidad[1]
            self.museoEntranceCost.text = latestValorEntrada
            
            let url = URL(string: latestThumbnail)!
            self.downloadImage(from: url)
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        museumInformationListener.remove()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "acerca_de_este_museo.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.museoImage.image = UIImage(data: data)
            }
        }
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
}
