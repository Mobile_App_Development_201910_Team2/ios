//
//  QRCodeViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class QRCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    let session = AVCaptureSession()
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var video = AVCaptureVideoPreviewLayer()
    var count = 0
    var userDisabilitySituation: String = ""
    var audioPlayer: AVAudioPlayer?
    var selectedMuseum: String = ""
    var selectedExhibition: String = ""
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "qrCodeAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "codigo_qr.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "qrCodeAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        // Defines capture device
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do
        {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }
        catch
        {
            print("ERROR")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        // Output is going to be processed on the main queue
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        // We are interested only on QR codes
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        session.startRunning()
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "codigo_qr.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "speechToText"
        {
            var secondViewController = segue.destination as! SpeechToTextViewController
            secondViewController.museum = selectedMuseum
            secondViewController.exhibition = selectedExhibition
        }
        else if segue.identifier == "exhibitionAudioPlayer"
        {
            var secondViewController = segue.destination as! ExhibitionAudioPlayerViewController
            secondViewController.museum = selectedMuseum
            secondViewController.exhibition = selectedExhibition
        }
        else if segue.identifier == "exhibitionText"
        {
            var secondViewController = segue.destination as! ExhibitionTextViewController
            secondViewController.museum = selectedMuseum
            secondViewController.exhibition = selectedExhibition
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    /**
     * Function that is called when there is an output
     */
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0
        {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            {
                if object.type == AVMetadataObject.ObjectType.qr
                {
                    // Se obtiene la situacion de discapacidad del usuario
                    userDisabilitySituation = obtainUserDisabilitySituation()
                    
                    // Se obtiene el tipo de exhibición
                    var museoYExhibicion = object.stringValue
                    var museoYExhibicionSeparados = museoYExhibicion!.split(separator: "/")
                    var museo = String(museoYExhibicionSeparados[0])
                    var exhibicion = String(museoYExhibicionSeparados[1])
                    var tipoExhibicion = String(museoYExhibicionSeparados[2])
                    
                    selectedMuseum = museo
                    selectedExhibition = exhibicion
                    
                    if userDisabilitySituation == "No puede oir" && tipoExhibicion == "audio"
                    {
                        // Speech to Text
                        self.performSegue(withIdentifier: "speechToText", sender: self)
                        self.session.stopRunning()
                    }
                    else if self.userDisabilitySituation == "No puede oir" && tipoExhibicion == "texto"
                    {
                        // Mostrar texto
                        self.performSegue(withIdentifier: "exhibitionText", sender: self)
                        self.session.stopRunning()
                    }
                    else if self.userDisabilitySituation == "No puede ver"
                    {
                        // Habilitar Audio
                        self.performSegue(withIdentifier: "exhibitionAudioPlayer", sender: self)
                        self.session.stopRunning()
                    }
                }
            }
        }
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func obtainUserDisabilitySituation() -> String
    {
        var answer:String = ""
        
        // Constants needed to read from Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInformation")
        request.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(request)
            
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let userDisabilitySituation = result.value(forKey: "disabilitySituation") as? String
                    {
                        answer = userDisabilitySituation
                        print(userDisabilitySituation)
                    }
                    if let userAge = result.value(forKey: "age") as? String
                    {
                        print(userAge)
                    }
                    if let userGender = result.value(forKey: "gender") as? String
                    {
                        print(userGender)
                    }
                }
            }
        }
        catch
        {
            print("Data could not be read from Core Data")
        }
        
        return answer
        
    }
}
