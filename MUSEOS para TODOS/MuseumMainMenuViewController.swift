//
//  MuseumMainMenuViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import FirebaseFirestore
import AVFoundation
import CoreData

class MuseumMainMenuViewController: UIViewController {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var museumTitle: UILabel!
    @IBOutlet weak var museumImage: UIImageView!
    @IBOutlet weak var acercaDeEsteMuseoButton: UIButton!
    @IBOutlet weak var exhibicionesButton: UIButton!
    @IBOutlet weak var exhibicionesNoVisitadasButton: UIButton!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var docRef: DocumentReference!
    var colRef: CollectionReference!
    var museumInformationListener: ListenerRegistration!
    var audioPlayer: AVAudioPlayer?
    var museum:String = ""
    var userSavedDisabilitySituation:String = ""
    var userSavedAge:String = ""
    var userSavedGender:String = ""
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "museumMainMenuAudioWasAlreadyPlayed") != nil)
        {            
            // Habilitar audio
            let path = Bundle.main.path(forResource: "museo_ios.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "museumMainMenuAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        museum = String(selectedMuseumName.split(separator: "/")[1])
        
        colRef = Firestore.firestore().collection("MuseosPT").document(museum).collection("Preferencias")
        
        // Look for the user information in Core Data
        
        // Constants needed to read from Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInformation")
        request.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(request)
            
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let userDisabilitySituation = result.value(forKey: "disabilitySituation") as? String
                    {
                        userSavedDisabilitySituation = userDisabilitySituation
                    }
                    if let userAge = result.value(forKey: "age") as? String
                    {
                        userSavedAge = userAge
                    }
                    if let userGender = result.value(forKey: "gender") as? String
                    {
                        userSavedGender = userGender
                    }
                }
            }
        }
        catch
        {
            print("Data could not be read from Core Data")
        }
        
        // Save user basic information into Firestore
        let idMobile = UIDevice.current.identifierForVendor!.uuidString
        let date = Date()
        let calendar = Calendar.current
        
        colRef.document("pref-\(idMobile)").setData([
            "idMobile": idMobile,
            "disabilitySituation": userSavedDisabilitySituation,
            "gender": userSavedGender,
            "age": userSavedAge,
            "day": calendar.component(.day, from: date),
            "month": calendar.component(.month, from: date),
            "year": calendar.component(.year, from: date)
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
        
        acercaDeEsteMuseoButton.layer.cornerRadius = 20
        acercaDeEsteMuseoButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        exhibicionesButton.layer.cornerRadius = 20
        exhibicionesButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
        exhibicionesNoVisitadasButton.layer.cornerRadius = 20
        exhibicionesNoVisitadasButton.backgroundColor = UserDefaults.standard.colorForKey(key: "Color")
                
        docRef = Firestore.firestore().document(selectedMuseumName)
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        museumInformationListener = docRef.addSnapshotListener { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let myData = docSnapshot.data()
            let latestMuseoTitle = myData?["nombre"] as? String ?? ""
            self.museumTitle.text = latestMuseoTitle
            
            let museumImageString = myData?["thumbnail"] as? String
            let url = URL(string: museumImageString!)
            let data = NSData(contentsOf: url!)
            if data != nil
            {
                self.museumImage.image = UIImage(data: data! as Data)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        museumInformationListener.remove()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "museo_ios.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "museumExhibitions"
        {
            var secondViewController = segue.destination as! ExhibitionsViewController
            secondViewController.museum = museum
        }
        else if segue.identifier == "notVisitedExhibitions"
        {
            var secondViewController = segue.destination as! NotVisitedExhibitionsViewController
            secondViewController.museum = museum
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
}
