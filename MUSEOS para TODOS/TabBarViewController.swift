//
//  TabBarViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import CoreData

class TabBarViewController: UITabBarController {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
        
    @IBOutlet weak var museosTabBar: UITabBar!
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var userCannotHear:Bool = false
    var userDoesNotRecognizeColor:Bool = false
    var userCannotSee:Bool = false
    var controllerArray = [UIViewController]()
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        museosTabBar.barTintColor = UserDefaults.standard.colorForKey(key: "TabBarColor")
        
        loadData()
                
        if userCannotHear
        {
            self.viewControllers?.removeAll()
                
            let controller1 = storyboard!.instantiateViewController(withIdentifier: "museums") as! NavigationControllerViewController
            let controller2 = storyboard!.instantiateViewController(withIdentifier: "qrCodeScanner") as! QRCodeViewController
            let controller3 = storyboard!.instantiateViewController(withIdentifier: "textToSpeech") as! TextToSpeechViewController
            let controller4 = storyboard!.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
                
            self.viewControllers?.append(controller1)
            self.viewControllers?.append(controller2)
            self.viewControllers?.append(controller3)
            self.viewControllers?.append(controller4)
        }
        
        if userDoesNotRecognizeColor
        {
            self.viewControllers?.removeAll()
                
            let controller1 = storyboard!.instantiateViewController(withIdentifier: "museums") as! NavigationControllerViewController
            let controller2 = storyboard!.instantiateViewController(withIdentifier: "qrCodeScanner") as! QRCodeViewController
            let controller3 = storyboard!.instantiateViewController(withIdentifier: "textToSpeech") as! TextToSpeechViewController
            let controller4 = storyboard!.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
                
            self.viewControllers?.append(controller1)
            self.viewControllers?.append(controller2)
            self.viewControllers?.append(controller3)
            self.viewControllers?.append(controller4)
        }
        
        if userCannotSee
        {
            self.viewControllers?.removeAll()
                
            let controller1 = storyboard!.instantiateViewController(withIdentifier: "museums") as! NavigationControllerViewController
            let controller2 = storyboard!.instantiateViewController(withIdentifier: "qrCodeScanner") as! QRCodeViewController
            let controller4 = storyboard!.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
                
            self.viewControllers?.append(controller1)
            self.viewControllers?.append(controller2)
            self.viewControllers?.append(controller4)
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func loadData()
    {
        // Constants needed to read from Core Data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInformation")
        request.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(request)
            
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let userDisabilitySituation = result.value(forKey: "disabilitySituation") as? String
                    {
                        if userDisabilitySituation.contains("No puede oir")
                        {
                            userCannotHear = true
                            userDoesNotRecognizeColor = false
                            userCannotSee = false
                        }
                        else if userDisabilitySituation.contains("No reconoce algunos colores")
                        {
                            userDoesNotRecognizeColor = true
                            userCannotHear = false
                            userCannotSee = false
                        }
                        else
                        {
                            userCannotSee = true
                            userCannotHear = false
                            userDoesNotRecognizeColor = false
                        }
                    }
                }
            }
        }
        catch
        {
            print("Data could not be read from Core Data")
        }
    }
}
