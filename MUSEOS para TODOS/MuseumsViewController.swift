//
//  MuseumsViewController.swift
//  MUSEOS para TODOS
//
//  Copyright © 2019 MobDevApps. All rights reserved.
//

import UIKit
import FirebaseFirestore
import AVFoundation

var selectedMuseumName = ""
var userNotInRecommendedMuseum = false

class MuseumsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //--------------------------------------------------
    // Outlets
    //--------------------------------------------------
    
    @IBOutlet weak var museumsTableView: UITableView!
    
    //--------------------------------------------------
    // Constants
    //--------------------------------------------------
    
    let reachability = Reachability()!
    let cellSpacingHeight: CGFloat = 5
    
    //--------------------------------------------------
    // Variables
    //--------------------------------------------------
    
    var colRef: CollectionReference!
    var userLatitude = 0.0
    var userLongitude = 0.0
    var userMuseum = ""
    var museums:[Museum] = []
    var newMuseum:Museum! = Museum(image: #imageLiteral(resourceName: "MUSEUM_1"), name: "example")
    var audioPlayer: AVAudioPlayer?
    
    //--------------------------------------------------
    // Lifecycle Functions
    //--------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLoadingAlert(Message: "Los datos de los museos están cargando.")
        
        museumsTableView.dataSource = self
        museumsTableView.delegate = self
        
        colRef = Firestore.firestore().collection("MuseosPT")
        
        colRef.getDocuments { (snapshot, error) in
            if error != nil
            {
                print(error)
            }
            else
            {
                for document in (snapshot?.documents)!
                {
                    if let museumName = document.data()["nombre"] as? String
                    {
                        let museumImageString = document.data()["thumbnail"] as? String
                        let url = URL(string: museumImageString!)
                        let data = NSData(contentsOf: url!)
                        if data != nil
                        {
                            self.newMuseum = Museum(image: UIImage(data: data! as Data)!, name: museumName)
                            self.museums.append(self.newMuseum)
                            self.museumsTableView.reloadSections([0], with: UITableView.RowAnimation.fade)
                        }
                    }
                }
            }
        }
        
        if UserDefaults.standard.bool(forKey: "UsuarioNoPuedeVer") && !(UserDefaults.standard.object(forKey: "museumsAudioWasAlreadyPlayed") != nil)
        {
            // Habilitar audio
            let path = Bundle.main.path(forResource: "lista_museos_ios.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
                UserDefaults.standard.set(true, forKey: "museumsAudioWasAlreadyPlayed")
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do
        {
            try reachability.startNotifier()
        }
        catch
        {
            print("Cound not start notifier")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !CheckInternet.Connection()
        {
            self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer?.stop()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let path = Bundle.main.path(forResource: "lista_museos_ios.wav", ofType: nil)!
            let url = URL(fileURLWithPath: path)
            
            do
            {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
    }
    
    //--------------------------------------------------
    // Functions
    //--------------------------------------------------
    
    func createAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // Yes
        alert.addAction(UIAlertAction(title: "Sí", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            selectedMuseumName = "MuseosPT/Museo-Militar"
            self.performSegue(withIdentifier: "museo", sender: self)
            
        }))
        
        // No
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            userNotInRecommendedMuseum = true
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func Alert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func internetChanged(note: Notification)
    {
        let reachability = note.object as! Reachability
        if reachability.connection == .none
        {
            DispatchQueue.main.async {
                // There is no Internet connection
                self.Alert(Message: "No hay conexión a Internet por lo que algunas de nuestras funcionalidades no están disponibles. Por favor conéctate lo más pronto para seguir disfrutando de todas las funcionalidades de MUSEOS para TODOS.")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return museums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let museum = museums[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MuseumsViewControllerTableViewCell
        cell.setMuseum(museum: museum)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("section: \(indexPath.section)")
        print("row: \(indexPath.row)")
        let selectedMuseum = museums[indexPath.row]
        print("clicked: \(selectedMuseum.name)")
        
        if selectedMuseum.name == "Museo Militar"
        {
            selectedMuseumName = "MuseosPT/Museo-Militar"
            
        }
        
        if selectedMuseum.name == "Planetario de Bogotá"
        {
            selectedMuseumName = "MuseosPT/Museo-Planetario"
        }
        
        if selectedMuseum.name == "Museo Uniandes"
        {
            selectedMuseumName = "MuseosPT/Museo3"
        }
        
        self.performSegue(withIdentifier: "museo", sender: self)
        
    }
    
    func createLoadingAlert(Message: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
