# MUSEOS para TODOS iOS App

Welcome to MUSEOS para TODOS iOS App!

![MUSEOS_para_TODOS](https://gitlab.com/Mobile_App_Development_201910_Team2/ios/raw/master/MUSEOS_para_TODOS.png)

## What is it?

MUSEOS para TODOS is an app that helps culture and enterntainment to be accessible to all.
It provides aids for people in a visual disability situation for them to enjoy museum's exhibitions.
Some of its features are

* Speech To Text
* Text To Speech
* Color adjustments

## How was it built?

This app was built using Xcode and Swift.

## Authors

*  Julio Poveda
*  Daniel Santiago Tapia

## Team Members

*  Rafael Cárdenas
*  David Cubillos
*  Julio Poveda
*  Daniel Santiago Tapia